﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunLibrary
{
    public abstract class IntUnarFunc
    {

        public abstract int Apply(int n);
    }
}
