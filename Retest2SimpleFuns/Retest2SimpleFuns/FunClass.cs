﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FunLibrary;

namespace Retest2SimpleFuns
{
    public class FunClass
    {
        private int a;

        public int calcFact(int a)
        {
            int result = 1;
            for(int i = a; i > 0; i--)
            {
                result *= i;
            }
            return result;
        }

        public int calcDegN(int a, int n)
        {
            int result = 1;
            for(int i = 0; i < n; i++)
            {
                result *= a;
            }
            return result;
        }

        public override string ToString() 
        {
            return Convert.ToString(a);
        }
    }
}
