﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FunLibrary;


namespace Retest2SimpleFuns

    {
        public class Proizvedenie
        {
            private int _a;

            public Proizvedenie(int a)
            {
                _a = a;
            }

            public int Proizv(int source)
            {
                return source * _a;
            }

            public string ToString()
            {
                return "(*" + _a.ToString() + ")";
            }
        }

    }

