﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FunLibrary;

namespace Retest2SimpleFuns
{
    public class divRemain:IntUnarFunc
    {
	    private int _x;

        public divRemain(int x)
        {
		    _x = x;
	    }

        public override int Apply(int source)
	    {
		    return source % _x;
	    }
	
	    public override string ToString() 
        {
		    return "(*" + _x.ToString() + ")";
	    }
    }

}
