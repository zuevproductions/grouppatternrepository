﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FunLibrary;

namespace Retest2SimpleFuns
{
    class Program
    {
        static void Main(string[] args)
        {
            const int n = 5;

            List<IntUnarFunc> collection = new List<IntUnarFunc>();
            collection.Add(new divRemain(3));
            

            Console.WriteLine("Enter number: ");
            string text = Console.ReadLine();

            int num;
            if (!int.TryParse(text, out num))
            {
                throw new ArgumentException("Wrong num format");
            }

            foreach (var fun in collection)
            {
                Console.Write(fun.ToString());
                num = fun.Apply(num);
                Console.WriteLine("=> {0}", num);
            }

            Console.ReadLine();
        }
    }
}
